from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from accounts.forms import UserRegistrationForm
# Create your views here.


def SignupView(request):
    if request.method == "GET":
        form = UserRegistrationForm()
        context = { "form": form}
        return render(request, "registration/signup.html", context)
    if request.method == "POST":
        form = UserRegistrationForm(data = request.POST)
        if form.is_valid():
            user = User.objects.create_user(username=request.POST['username'], password=request.POST['password1'])
            user = authenticate(username=user.username, password=request.POST['password1'])
            login(request, user)
            return redirect('home')
        else:
            form = UserRegistrationForm()
            context = { "form": form}
            return render(request, 'registration/signup.html', context)

