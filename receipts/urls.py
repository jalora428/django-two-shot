from django.urls import path
from receipts.views import (AccountCreateView, AccountListView, ExpenseCategoryCreateView, ExpenseCategoryListView, ReceiptListView, ReceiptCreateView)

urlpatterns = [
    path(route="", view=ReceiptListView.as_view(), name="home"),
    path(route="create/", view=ReceiptCreateView.as_view(), name="create_receipt"),
    path(route="categories/", view=ExpenseCategoryListView.as_view(), name="expense_category"),
    path(route="accounts/", view=AccountListView.as_view(), name="account_list"),
    path(route="categories/create/", view=ExpenseCategoryCreateView.as_view(), name="create_category"),
    path(route="accounts/create/", view=AccountCreateView.as_view(), name="create_account"),
    ]

